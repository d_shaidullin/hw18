﻿#include <iostream>
#include <string>
#include <stdlib.h>
#include <cassert> 
using namespace std;


template <typename T>

class Stack
{
public:

    Stack():StackSize()
    {}

    Stack(int _StackSize): StackSize(_StackSize)
    {
        Stack_list = new T[_StackSize];
    }

    ~Stack()
    {
        delete[] Stack_list;
    }
    
    void push(T element)
    {
        assert(stack_index <= StackSize);
        Stack_list[stack_index] = element;
        stack_index++;
    }

    T pop()
    {
        assert(stack_index > 0);
        stack_index--;
        return Stack_list[stack_index];
    }

    T TopElement()
    {
        assert(stack_index > 0);
        return Stack_list[(stack_index - 1)];
    }

    
private:
    int StackSize = 0;
    int stack_index = 0;
    T* Stack_list;
};





int main()
{
    setlocale(LC_ALL, "Russian");
    int size;

    cout << "Сколько элементов вы хотите добавить в стек? - ";
    cin >> size; cout << '\n';
    
    
    Stack<float> object(size);
	
	cout << "Какие элементы вы хотите добавить в стек?\n";
    float element;
	for (int i = 0; i < size; i++)
	{
		cin >> element;
		object.push(element);
	}

    cout << endl;
    cout << "Верхний элемент стека: \n";
    cout << object.TopElement() << " "; cout << endl;
   
	cout << "Итоговый стек:\n";
	for (int i = 0; i < size; i++)
	{
		cout << object.pop() << " ";
	}
	
 
    
    
	return 0;
    
  
}
